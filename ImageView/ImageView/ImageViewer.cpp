#include <opencv2\highgui.hpp>

void main()
{
	IplImage* imgV = cvLoadImage("img1.jpg");
	cvNamedWindow("Imageviewer", CV_WINDOW_AUTOSIZE);
	cvShowImage("Imageviewer", imgV);
	cvWaitKey(0);
	cvReleaseImage(&imgV);
	cvDestroyWindow("Imageviewer");
}
